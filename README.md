# Schneider electric - NUWE hackaton

## Hecho por:

### Data science:

- Bruno Moya [@elblogbruno](https://github.com/elblogbruno)
- Joel Marco Quiroga [@joelqp](https://github.com/joelqp)
- Marc Alfonso

### Cybersecurity

- Gabriel Juan [@GabrielJuan349](https://github.com/GabrielJuan349)
- Anthony Michael Alonso [@AMAlonso64](https://github.com/AMAlonso64)
- Sergi Morales [@Mofitex](https://github.com/Mofitex)